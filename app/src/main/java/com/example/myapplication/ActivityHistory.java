package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.myapplication.model.BookingRequest;
import com.example.myapplication.screen.HistoryAdapter;
import com.example.myapplication.screen.MyHolder;
import com.example.myapplication.screen.home_screen;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ActivityHistory extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter mAdapter;
    List<BookingRequest> bookingRequestList = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();

    static final String TAG = "ActivityHistory";

    private FirebaseUser currentUser;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        recyclerView = findViewById(R.id.container_recyclerview_history);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        mFirebaseAuth = FirebaseAuth.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        root = FirebaseDatabase.getInstance().getReference();

        String uuid = mFirebaseAuth.getCurrentUser().getUid();
        DatabaseReference mReference = FirebaseDatabase.getInstance().getReference("User")
                        .child(uuid).child("History");

        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren()){
                    BookingRequest bookingRequest = dataSnapshot.getValue(BookingRequest.class);
                    String getRoomName = bookingRequest.getRoomNameReq();
                    String getStartTime = bookingRequest.getStartTimeReq();
                    String getEndTime = bookingRequest.getEndTimeReq();
                    String getBookingDate = bookingRequest.getDateReq();

                    Log.d(TAG, "getRoomName : "+getRoomName);
                    Log.d(TAG, "getStartTime : "+getStartTime);
                    Log.d(TAG, "getEndTime : "+getEndTime);
                    Log.d(TAG, "getBookingDate : "+getBookingDate);
                    Log.d(TAG, "List data book request : "+bookingRequest);
                    bookingRequestList.add(bookingRequest);
                    mAdapter = new HistoryAdapter(bookingRequestList, getApplication());
                    mAdapter.notifyDataSetChanged();
                    recyclerView.setAdapter(mAdapter);
                }
                Log.d(TAG,String.valueOf(snapshot.getChildrenCount()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e(TAG, error.getMessage());
            }
        });


        //------------------------ Delete By Swipe To Right And Left
//        FirebaseDatabase.getInstance().getReference()
//                .child("notifications").child(toId.toString()).removeValue()
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Log.d("Delete", "Notification has been deleted");
//                        } else {
//                            Log.d("Delete", "Notification couldn't be deleted");
//                        }
//                    }
//                });

    }
}