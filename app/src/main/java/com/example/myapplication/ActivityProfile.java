package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myapplication.model.User;
import com.example.myapplication.screen.Login_Screen;
import com.example.myapplication.screen.MyHolder;
import com.example.myapplication.screen.home_screen;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class ActivityProfile extends AppCompatActivity {

    FirebaseUser currentUser;
    FirebaseDatabase mDatabase;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mReference;
    AuthCredential mAuthCredential;

    TextInputLayout mUsername, mEmail, mPhone, mPassword, mOldPassword;
    Button mBtnSave, mBtnCancel;

    static final String TAG = "User ";

    String getEmail;
    String getUserName;
    String getPhone;
    String getPassword;
    String uuid;

    String inputUsername;
    String inputEmail;
    String inputPhone;
    String inputPassword;

    String oldEmail;
    String oldPassword;
    String newEmail;
    String newPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mUsername = findViewById(R.id.username_edit);
        mEmail = findViewById(R.id.email_edit);
        mPhone = findViewById(R.id.phone_edit);
        mPassword = findViewById(R.id.password_edit);
        mOldPassword = findViewById(R.id.old_password_edit);
        mBtnSave = findViewById(R.id.btn_save_edit);
        mBtnCancel = findViewById(R.id.btn_cancel_edit);

        mFirebaseAuth = FirebaseAuth.getInstance();
        currentUser = mFirebaseAuth.getCurrentUser();
        mFirebaseAuth.getCurrentUser();

        if (currentUser != null) {
            uuid = currentUser.getUid();
        }

        /*
        TODO : Get data from real time
         */
        mReference = FirebaseDatabase.getInstance().getReference().child("User");
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                getEmail = snapshot.child(currentUser.getUid()).child("email").getValue(String.class);
                getUserName = snapshot.child(currentUser.getUid()).child("name").getValue(String.class);
                getPhone = snapshot.child(currentUser.getUid()).child("phone").getValue(String.class);
//                getPassword = snapshot.child(currentUser.getUid()).child("password").getValue(String.class);

                mEmail.getEditText().setText(getEmail);
                mUsername.getEditText().setText(getUserName);
                mPhone.getEditText().setText(getPhone);
//                mPassword.getEditText().setText(getPassword);

                Log.d(TAG, "Email : " + getEmail + " Username : " + getUserName + " Phone : " + getPhone + " Password : " + getPassword);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e(TAG, "loadPost:onCancelled", error.toException());
            }

        });

        Log.d(TAG, "ActivityProfile : " + getPassword);
        Log.d(TAG, "ActivityProfile : " + mPassword.getEditText().getText());


        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    DatabaseReference mReference = FirebaseDatabase.getInstance().getReference("User").child(uuid);

                    inputUsername = String.valueOf(mUsername.getEditText().getText());
                    inputEmail = String.valueOf(mEmail.getEditText().getText());
                    inputPhone = String.valueOf(mPhone.getEditText().getText());
                    inputPassword = String.valueOf(mPassword.getEditText().getText());

                    if (inputUsername.isEmpty() || inputEmail.isEmpty() || inputPhone.isEmpty() || inputPassword.isEmpty()) {
                        Toast.makeText(ActivityProfile.this, "Please check email and password!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    User user = new User(inputUsername, inputEmail, inputPassword, inputPhone);
                    mReference.setValue(user);
                /*
                TODO :  Update data in Authentication Sign in With EmailAndPassword and
                        intent get old email and password from register
                 */
                    oldEmail = currentUser.getEmail();
                    oldPassword = String.valueOf(mOldPassword.getEditText().getText());

                    newEmail = String.valueOf(mEmail.getEditText().getText()).trim();
                    newPassword = String.valueOf(mPassword.getEditText().getText()).trim();

                    Log.d(TAG, "ActivityProfile : " + oldEmail);
//                Log.d(TAG,"ActivityProfile : "+oldPassword);
                    Log.d(TAG, "ActivityProfile : " + newPassword);


                    if (oldEmail.isEmpty()) {
                        Toast.makeText(ActivityProfile.this, "Please check email.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (oldPassword.isEmpty()) {
                        Toast.makeText(ActivityProfile.this, "Please check password.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (newPassword.isEmpty()) {
                        Toast.makeText(ActivityProfile.this, "Please check password.", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    mAuthCredential = EmailAuthProvider.getCredential(oldEmail, oldPassword);
                    currentUser.reauthenticate(mAuthCredential).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                    /*
                    TODO: Update Email
                     */
                                currentUser.updateEmail(newEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(ActivityProfile.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                    /*
                    TODO: Update Password
                     */
                                currentUser.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (!task.isSuccessful()) {
                                            Toast.makeText(ActivityProfile.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });

                                Toast.makeText(ActivityProfile.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ActivityProfile.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(ActivityProfile.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

//                Toast.makeText(ActivityProfile.this,"Save Change Successfully.", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(ActivityProfile.this, home_screen.class);
//                startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void updateUser() {

    }
}