package com.example.myapplication.screen;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.HallRoom;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.MessageFormat;
import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


public class booking extends DialogFragment {

    TextView startTime,endTime,date,roomNamek;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog,timePickerDialog1;
    private int CalendarHour, CalendarMinute,CalendarHour1,CalendarMinute1;
    Calendar calendar,calendar1;
    String format,format1;
    DatabaseReference databaseReference;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_booking, container, false);

        startTime = v.findViewById(R.id.textViewStart);
        endTime = v.findViewById(R.id.textViewEnd);
        roomNamek = v.findViewById(R.id.textViewRoomName);
        date = v.findViewById(R.id.textViewDate);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Room");
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String roomNameKey = bundle.getString("roomKey");
            databaseReference.child(String.valueOf(roomNameKey)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    if (snapshot.exists()) {
                        HallRoom hallRoom = snapshot.getValue(HallRoom.class);
                        assert hallRoom != null;
                        roomNamek.setText(hallRoom.getRoomName());
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

        date.setOnClickListener(v13 -> {
            final Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH);
            int year = calendar.get(Calendar.YEAR);
            //date picker dialog
            datePickerDialog = new DatePickerDialog(getContext(),
                    (view, year1, month1, dayOfMonth) -> date.setText(dayOfMonth + "/" + (month1 + 1) + "/" + year1), year, month, day);
            datePickerDialog.show();
        });
        startTime.setOnClickListener(v1 -> {
            calendar = Calendar.getInstance();
            CalendarHour = calendar.get(Calendar.HOUR_OF_DAY);
            CalendarMinute = calendar.get(Calendar.MINUTE);


            timePickerDialog = new TimePickerDialog(getActivity(),
                    (view, hourOfDay, minute) -> {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        }
                        else if (hourOfDay == 12) {

                            format = "PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        }
                        else {

                            format = "AM";
                        }
                        String minutes;
                        if (minute < 10)
                            minutes = "0" + minute;
                        else
                            minutes = String.valueOf(minute);
                        startTime.setText(MessageFormat.format("{0}:{1}{2}", hourOfDay, minutes, format));
                    }, CalendarHour, CalendarMinute, false);
            timePickerDialog.show();

        });
        endTime.setOnClickListener(v12 -> {
            calendar1 = Calendar.getInstance();
            CalendarHour1 = calendar1.get(Calendar.HOUR_OF_DAY);
            CalendarMinute1 = calendar1.get(Calendar.MINUTE);

            timePickerDialog1 = new TimePickerDialog(getActivity(),
                    (view, hourOfDay, minute) -> {

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format1 = "AM";
                        }
                        else if (hourOfDay == 12) {

                            format1 = "PM";

                        }
                        else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format1 = "PM";

                        }
                        else {

                            format1 = "AM";
                        }
                        String minutes;
                        if (minute < 10)
                            minutes = "0" + minute;
                        else
                            minutes = String.valueOf(minute);
                        endTime.setText(MessageFormat.format("{0}:{1}{2}", hourOfDay, minutes, format1));
                    }, CalendarHour1, CalendarMinute1, false);
            timePickerDialog1.show();

        });
        // Inflate the layout for this fragment
        return v;
    }
}