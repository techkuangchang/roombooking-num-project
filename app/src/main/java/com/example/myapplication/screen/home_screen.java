package com.example.myapplication.screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.example.myapplication.ActivityHistory;
import com.example.myapplication.ActivityProfile;
import com.example.myapplication.R;
import com.example.myapplication.model.BookingRequest;
import com.example.myapplication.model.HallRoom;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class home_screen extends AppCompatActivity {
//    FirebaseRecyclerAdapter<HallRoom,MyHolder> adapter;
    FirebaseRecyclerOptions<HallRoom> hallRooms;
    DatabaseReference reference;
    private Toolbar toolbar;
    private List<HallRoom> hallRoomList;
    private Button btn;
    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView mTxtUsername, mTxtEmail;
    private ProgressBar mProgressBar;
    DatePickerDialog datePicker;
    TimePickerDialog picker;

    private FirebaseUser currentUser;
    private FirebaseDatabase mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private RecyclerView.Adapter mAdapter;
    MyHolder.OnItemClickListener mOnItemClickListener;

    static final String TAG = "HomeScreen";
    String startTime, endTime, bookingDate;
    BookingRequest bookingRequest;
    Date date;
    SimpleDateFormat newDateFormat;
    String finalDate;
    int maxId = 0;

    String am_pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);

        navigationView = findViewById(R.id.nav_view);
        drawerLayout = findViewById(R.id.drawer);
        mProgressBar = findViewById(R.id.progress_bar_home);

        navigationView.bringToFront();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open_navigation_drawer,R.string.close_navigation_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.light_red));
        navigationView.setNavigationItemSelectedListener(this::onOptionsItemSelected);

        mFirebaseAuth = FirebaseAuth.getInstance();

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        /*
        TODO: Initialize data set to Firebase Real time
         */
        hallRoomList = new ArrayList<>();
        hallRoomList.add(new HallRoom("VIP Room","សាលផ្ការំដួល","Hall 1","500",R.drawable.p1));
        hallRoomList.add(new HallRoom("VIP Room","សាលផ្កាម្លិះ","Hall 2","400",R.drawable.p2));
        hallRoomList.add(new HallRoom("Normal Room","សាលផ្កាត្រកួន","Hall 3","200",R.drawable.p3));
        hallRoomList.add(new HallRoom("Normal Room","សាលផ្ការំចង់","Hall 4","100",R.drawable.p4));

        reference = FirebaseDatabase.getInstance().getReference().child("Rooms");
        reference.setValue(hallRoomList);

        /*
        TODO: Handle Click RecyclerView to Booking
         */
        mOnItemClickListener = new MyHolder.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG,"onRecyclerView : clicked "+position);
                Context context;
                CurrentUserBooking(position);
            }
        };

        mAdapter = new MyHolder(hallRoomList, this, mOnItemClickListener);
        recyclerView.setAdapter(mAdapter);

        /*
        TODO: Get current user and set text to drawer
         */
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        View headerView = navigationView.getHeaderView(0);
        TextView username = headerView.findViewById(R.id.name);
        TextView email = headerView.findViewById(R.id.email);

        DatabaseReference mReference = FirebaseDatabase.getInstance().getReference().child("User");
        mReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String getEmail = snapshot.child(currentUser.getUid()).child("email").getValue(String.class);
                String getUserName = snapshot.child(currentUser.getUid()).child("name").getValue(String.class);
                email.setText(getEmail);
                username.setText(getUserName);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.e(TAG, error.getMessage());
            }
        });

    }

    protected boolean checkDateValidity(Date date){
        Calendar cal = new GregorianCalendar(date.getYear() + 1900, date.getMonth(), date.getDate(), date.getHours(), date.getMinutes());
        if((cal.getTimeInMillis()-System.currentTimeMillis()) <= 0){
            return false;
        }else{
            return true;
        }
    }

    /*
    TODO: Booking Alert Dialog
     */
    private void CurrentUserBooking(int position) {

        TextInputLayout mInputEditTextRoomName, mInputEditTextRoomType, mInputEditTextHallName;
        EditText mInputEditTextStartTime, mInputEditTextEndTime, mInputEditTextDate;
        Button mBtnBooking, mBtnCancelBooking;
        TextView mTxtHint;

        ViewGroup viewGroup = findViewById(R.id.home_fragment);
        View view = LayoutInflater.from(this).inflate(R.layout.my_custom_booking, viewGroup, false);

        mInputEditTextRoomName = view.findViewById(R.id.room_name_booking);
        mInputEditTextRoomType = view.findViewById(R.id.room_type_booking);
        mInputEditTextHallName = view.findViewById(R.id.hall_name_booking);
        mInputEditTextStartTime = view.findViewById(R.id.start_time_booking);
        mInputEditTextEndTime = view.findViewById(R.id.end_time_booking);
        mInputEditTextDate = view.findViewById(R.id.date_booking_date);
        mBtnBooking = view.findViewById(R.id.btn_booking);
        mBtnCancelBooking = view.findViewById(R.id.btn_cancel_booking);
        mTxtHint = view.findViewById(R.id.text_hint);

        /*
        TODO: Get Realtime Data From Path Rooms and Set to EditText
         */
        DatabaseReference mReference = FirebaseDatabase.getInstance().getReference().child("Rooms");

        if (position == 0) {
            mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String getRoomName = snapshot.child("0").child("roomName").getValue(String.class);
                    String getRoomType = snapshot.child("0").child("roomType").getValue(String.class);
                    String getHallName = snapshot.child("0").child("hallName").getValue(String.class);

                    Log.d(TAG, "Room Name : "+getRoomName);
                    Log.d(TAG, "Room Type : "+getRoomType);
                    Log.d(TAG, "Hall Name : "+getHallName);

                    mInputEditTextRoomName.getEditText().setText(getRoomName);
                    mInputEditTextRoomType.getEditText().setText(getRoomType);
                    mInputEditTextHallName.getEditText().setText(getHallName);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
        }
        else if (position == 1) {
            mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String getRoomName = snapshot.child("1").child("roomName").getValue(String.class);
                    String getRoomType = snapshot.child("1").child("roomType").getValue(String.class);
                    String getHallName = snapshot.child("1").child("hallName").getValue(String.class);

                    Log.d(TAG, "Room Name : "+getRoomName);
                    Log.d(TAG, "Room Type : "+getRoomType);
                    Log.d(TAG, "Hall Name : "+getHallName);

                    mInputEditTextRoomName.getEditText().setText(getRoomName);
                    mInputEditTextRoomType.getEditText().setText(getRoomType);
                    mInputEditTextHallName.getEditText().setText(getHallName);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
        }
        else if (position == 2) {
            mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String getRoomName = snapshot.child("2").child("roomName").getValue(String.class);
                    String getRoomType = snapshot.child("2").child("roomType").getValue(String.class);
                    String getHallName = snapshot.child("2").child("hallName").getValue(String.class);

                    Log.d(TAG, "Room Name : "+getRoomName);
                    Log.d(TAG, "Room Type : "+getRoomType);
                    Log.d(TAG, "Hall Name : "+getHallName);

                    mInputEditTextRoomName.getEditText().setText(getRoomName);
                    mInputEditTextRoomType.getEditText().setText(getRoomType);
                    mInputEditTextHallName.getEditText().setText(getHallName);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
        }
        else if (position == 3) {
            mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    String getRoomName = snapshot.child("3").child("roomName").getValue(String.class);
                    String getRoomType = snapshot.child("3").child("roomType").getValue(String.class);
                    String getHallName = snapshot.child("3").child("hallName").getValue(String.class);

                    Log.d(TAG, "Room Name : "+getRoomName);
                    Log.d(TAG, "Room Type : "+getRoomType);
                    Log.d(TAG, "Hall Name : "+getHallName);

                    mInputEditTextRoomName.getEditText().setText(getRoomName);
                    mInputEditTextRoomType.getEditText().setText(getRoomType);
                    mInputEditTextHallName.getEditText().setText(getHallName);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.e(TAG, error.getMessage());
                }
            });
        }
        else {
            Log.d(TAG, "Position is NULL");
        }

        /*
        TODO: TimePicker Dialog Start Time
         */
        mInputEditTextStartTime.setInputType(InputType.TYPE_NULL);
        mInputEditTextStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minutes = calendar.get(Calendar.MINUTE);

                // Start time picker dialog
                picker = new TimePickerDialog(home_screen.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {

                                if (sHour == 0) {

                                    sHour += 12;

                                    am_pm = "AM";
                                }
                                else if (sHour == 12) {

                                    am_pm = "PM";

                                }
                                else if (sHour > 12) {

                                    sHour -= 12;

                                    am_pm = "PM";

                                }
                                else {

                                    am_pm = "AM";
                                }

                                String minutes;
                                if (sMinute < 10)
                                    minutes = "0" + sMinute;
                                else
                                    minutes = String.valueOf(sMinute);

                                mInputEditTextStartTime.setText(sHour + ":" + minutes + " " + am_pm);
                                startTime = (sHour + ":" + sMinute + " " + am_pm);
                                Log.d(TAG,"Start : time "+startTime);
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });

        /*
        TODO: TimePicker Dialog End Time
         */
        mInputEditTextEndTime.setInputType(InputType.TYPE_NULL);
        mInputEditTextEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar cldr = Calendar.getInstance();
                int hour = cldr.get(Calendar.HOUR_OF_DAY);
                int minutes = cldr.get(Calendar.MINUTE);

                // End time picker dialog
                picker = new TimePickerDialog(home_screen.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker tp, int sHour, int sMinute) {

                                if (sHour == 0) {

                                    sHour += 12;

                                    am_pm = "AM";
                                }
                                else if (sHour == 12) {

                                    am_pm = "PM";

                                }
                                else if (sHour > 12) {

                                    sHour -= 12;

                                    am_pm = "PM";

                                }
                                else {

                                    am_pm = "AM";
                                }

                                String minutes;
                                if (sMinute < 10)
                                    minutes = "0" + sMinute;
                                else
                                    minutes = String.valueOf(sMinute);

                                mInputEditTextEndTime.setText(sHour + ":" + minutes + " " + am_pm);
                                endTime = (sHour + ":" + sMinute + " " + am_pm);
                                Log.d(TAG,"End : time "+endTime);
                            }
                        }, hour, minutes, true);
                picker.show();
            }
        });

        /*
        TODO: DatePicker Dialog
         */
        mInputEditTextDate.setInputType(InputType.TYPE_NULL);
        mInputEditTextDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                int month = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);
                //date picker dialog
                datePicker = new DatePickerDialog(home_screen.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                mInputEditTextDate.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                                bookingDate = (dayOfMonth + "/" + (month + 1) + "/" + year);

                                String dateValidate = mInputEditTextDate.getText().toString();
                                DateFormat format = new SimpleDateFormat("dd/M/yyyy");
                                try {
                                    date = format.parse(dateValidate);
                                    newDateFormat = new SimpleDateFormat("yyyy-MM-DD");
                                    finalDate = newDateFormat.format(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                                    LocalDate date = LocalDate.now();

                                    if (finalDate.equals(date.toString())) {
                                        Toast.makeText(getApplicationContext(),"Booking fail!, Please choose future date.", Toast.LENGTH_SHORT).show();
                                        mInputEditTextDate.setText("");
                                    }
                                    else {
                                        mTxtHint.setText("Is is future date ?");
                                    }

                                    Log.d(TAG, "LocalDate : "+date.toString());
                                    Log.d(TAG, "FinalFormat : "+finalDate);
                                }

                            }
                        },year,month,day);
                datePicker.show();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        mBtnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((mInputEditTextStartTime.getText().toString().isEmpty())){
                    Toast.makeText(getApplicationContext(),"Start Time can't empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if((mInputEditTextEndTime.getText().toString().isEmpty())){
                    Toast.makeText(getApplicationContext(),"End Time can't empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if((mInputEditTextDate.getText().toString().isEmpty())){
                    Toast.makeText(getApplicationContext(),"Book Date can't empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                
                mTxtHint.setText("");
                /*
                TODO: Create New Path Child History in Firebase By CurrentUser Booking
                 */
                if(position == 0){
                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String getRoomName = snapshot.child("0").child("roomName").getValue(String.class);
                            Log.d(TAG, "Room Name : "+getRoomName);

                            InsertHistory(getRoomName);

//                            LocalDate date = LocalDate.now();
                            Toast.makeText(getApplicationContext(),"Booking Room Successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.e(TAG, error.getMessage());
                        }
                    });
                }
                else if(position == 1){
                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String getRoomName = snapshot.child("1").child("roomName").getValue(String.class);

                            InsertHistory(getRoomName);

                            Toast.makeText(getApplicationContext(),"Booking Room Successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.e(TAG, error.getMessage());
                        }
                    });
                }
                else if(position == 2){
                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String getRoomName = snapshot.child("2").child("roomName").getValue(String.class);

                            InsertHistory(getRoomName);

                            Toast.makeText(getApplicationContext(),"Booking Room Successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.e(TAG, error.getMessage());
                        }
                    });
                }
                else if(position == 3){
                    mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            String getRoomName = snapshot.child("3").child("roomName").getValue(String.class);

                            InsertHistory(getRoomName);

                            Toast.makeText(getApplicationContext(),"Booking Room Successfully", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Log.e(TAG, error.getMessage());
                        }
                    });
                }

                alertDialog.dismiss();
            }
        });

        mBtnCancelBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    private void ValidateBooking(TextInputLayout mInputEditTextStartTime, TextInputLayout mInputEditTextEndTime,
                                 TextInputLayout mInputEditTextDate){

    }

    private void ClearBooking(TextInputLayout mInputEditTextStartTime, TextInputLayout mInputEditTextEndTime,
                              TextInputLayout mInputEditTextDate){

    }

    private void InsertHistory(String getRoomName) {
        mFirebaseAuth = FirebaseAuth.getInstance();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String uuid = mFirebaseAuth.getCurrentUser().getUid();
        bookingRequest = new BookingRequest();

        DatabaseReference mReference = FirebaseDatabase.getInstance().getReference("User")
                .child(uuid).child("History");
        mReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    maxId = (int) snapshot.getChildrenCount();
                }
                else {
                    //
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        bookingRequest.setRoomNameReq(getRoomName);;
        bookingRequest.setStartTimeReq(startTime);
        bookingRequest.setEndTimeReq(endTime);
        bookingRequest.setDateReq(bookingDate);

        mReference.child(String.valueOf(maxId+1)).setValue(bookingRequest);
        Log.d(TAG,"Data book request : "+bookingRequest);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.homepf :
                System.out.println("Home");
                break;
            case R.id.profile :
                Intent intent = new Intent(this, ActivityProfile.class);
                startActivity(intent);
                break;
            case R.id.history :
                System.out.println("History");
                Intent intentHistory = new Intent(this, ActivityHistory.class);
                startActivity(intentHistory);
                break;
            case R.id.logout :
                /*
                 * TODO: User Sign out
                 */
                System.out.println("Log Out");
                FirebaseAuth.getInstance().signOut();
                Intent logoutIntent = new Intent(this,Login_Screen.class);
                startActivity(logoutIntent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

}