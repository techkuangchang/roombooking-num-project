package com.example.myapplication.screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.ActivityProfile;
import com.example.myapplication.R;
import com.example.myapplication.data.DatabaseHelper;
import com.example.myapplication.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Signup_Screen extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private EditText eFullName, eEmail, ePassword, ePhone;

    Login_Screen login_screen = new Login_Screen();

    static final String TAG = "SignUp";

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
//            onBackPressed();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void RegisterUser() {
        String name, email, phone, password;
        name = eFullName.getText().toString().trim();
        email = eEmail.getText().toString().trim();
        phone = ePhone.getText().toString();
        password = ePassword.getText().toString().trim();

        if (name.isEmpty()) {
            eFullName.setError("Name is required.");
            eFullName.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            eEmail.setError("Email is required.");
            eEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            eEmail.setError("Please provide valid email.");
            eEmail.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            ePhone.setError("Phone is required.");
            ePhone.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            ePassword.setError("Password is required.");
            ePassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            ePassword.setError("Password must be 6 characters or more than.");
            ePassword.requestFocus();
            return;
        }

        /*
        TODO: Get old password and old email from register pass to update user
         */
//        Intent intent = new Intent(this, ActivityProfile.class);
//        intent.putExtra("OldEmail", email);
//        intent.putExtra("OldPassword", password);
//        startActivity(intent);

        boolean connect = login_screen.isConnected(this);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        User user = new User(name, email, password, phone);
                        FirebaseDatabase.getInstance().getReference("User")
                                .child(FirebaseAuth.getInstance().getUid())
                                .setValue(user);

                                Toast.makeText(Signup_Screen.this, "Register has been successful", Toast.LENGTH_SHORT).show();
                                Log.d(TAG,"Data : "+user);
                                Log.d(TAG,"Data : "+user.getName());

//                                Intent passToDrawer = new Intent(this, home_screen.class);
//                                passToDrawer.putExtra("EmailKey",user.getEmail());
//                                passToDrawer.putExtra("UsernameKey",user.getName());
//                                startActivity(passToDrawer);
//                                       progressBar.clearAnimation();
                                startActivity(new Intent(Signup_Screen.this, Login_Screen.class));
                    } else if(!connect) {
                        Toast.makeText(Signup_Screen.this, "Failed to register, Please check your internet connection!", Toast.LENGTH_SHORT).show();

                    }
                });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup__screen);
        mAuth = FirebaseAuth.getInstance();

        Button btnSignupPage = findViewById(R.id.btnSignupPage);
        eFullName = findViewById(R.id.eFullName);
        eEmail = findViewById(R.id.eEmail);
        ePassword = findViewById(R.id.ePassword);
        ePhone = findViewById(R.id.ePhone);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(v -> {

            Intent login_screen = new Intent(Signup_Screen.this, Login_Screen.class);
            startActivity(login_screen);
            onBackPressed();
            finish();
        });

        btnSignupPage.setOnClickListener(v -> {

            RegisterUser();
//            databaseHelper = new DatabaseHelper(this);
//            fullName = eFullName.getText().toString();
//            email = eEmail.getText().toString();
//            password = ePassword.getText().toString();
//            phone = ePhone.getText().toString();
//            if (fullName.equals("") || email.equals("") || password.equals("") || phone.equals("")) {
//                Toast.makeText(getApplicationContext(), "Field can not empty", Toast.LENGTH_SHORT).show();
//
//
//            } else {
//                Boolean checkMail = databaseHelper.checkMail(email);
//                if (checkMail == true) {
//                    Boolean insert = databaseHelper.insertDB(fullName, email, password, phone);
//                    if (insert == true) {
//                        Toast.makeText(getApplicationContext(), "Register Success", Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(Signup_Screen.this, Login_Screen.class));
//                        finish();
//                    }
//                } else {
//                    Toast.makeText(getApplicationContext(), "Email is Already exists!", Toast.LENGTH_SHORT).show();
//                }
//
//            }


        });

    }


}