package com.example.myapplication.screen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.model.HallRoom;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyHolder extends RecyclerView.Adapter<MyHolder.MyViewHolder> {

    private List<HallRoom> roomList;
    private Context context;

    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public MyHolder(List<HallRoom> roomList, Context context, OnItemClickListener mOnItemClickListener) {
        this.roomList = roomList;
        this.context = context;
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @NonNull
    @Override
    public MyHolder.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder.MyViewHolder holder, int position) {
        HallRoom hallRoom = roomList.get(position);
        holder.hallName.setText(hallRoom.getHallName());
        holder.roomName.setText(hallRoom.getRoomName());
        holder.roomType.setText(hallRoom.getRoomType());
        holder.capacity.setText(hallRoom.getCapacity());
        Picasso.get().load(hallRoom.getImage()).into(holder.imageView);

        /*
        TODO: Handle click RecyclerView to booking
         */
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Holder Clicked");
                mOnItemClickListener.onItemClick(v,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView hallName;
        public TextView capacity;
        public ImageView imageView;
        public TextView roomName;
        public TextView roomType;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            hallName = itemView.findViewById(R.id.txtHallName);
            capacity = itemView.findViewById(R.id.txtCapacity);
            imageView = itemView.findViewById(R.id.imageViewHall);
            roomName = itemView.findViewById(R.id.room_name);
            roomType = itemView.findViewById(R.id.room_title);
        }
    }

}
