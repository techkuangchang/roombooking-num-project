package com.example.myapplication.screen;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.myapplication.ActivityProfile;
import com.example.myapplication.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login_Screen extends AppCompatActivity {
    private EditText tfEmail, tfPassword;
    private Button btnLogin, btnSignup;
    private FirebaseAuth mAuth;
    DatabaseReference mReference;

    String getPassword;
    String email, password;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_login__screen);

        tfEmail = findViewById(R.id.tfEmail);
        tfPassword = findViewById(R.id.tfPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnSignup = findViewById(R.id.btnSignup);
        mProgressBar = findViewById(R.id.progress_bar);

        btnSignup.setOnClickListener(v -> {
            Intent signupIntern = new Intent(Login_Screen.this, Signup_Screen.class);
            startActivity(signupIntern);
            finish();
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
    }

    public  boolean isConnected(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiConn = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if(networkInfo !=null && networkInfo.isConnected() || wifiConn != null && wifiConn.isConnected()){
            return true;
        }else {
            return false;
        }
    }

    private void Login() {
        email = tfEmail.getText().toString().trim();
        password = tfPassword.getText().toString().trim();

        if (email.isEmpty()) {
            tfEmail.setError("Please enter your email.");
            tfEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            tfPassword.setError("Please enter your password.");
            tfPassword.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            tfEmail.setError("Please provide your email.");
            tfEmail.requestFocus();
            return;
        }

        mProgressBar.setVisibility(View.VISIBLE);

        FirebaseUser user = mAuth.getCurrentUser();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if(user != null) {
                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(Login_Screen.this, home_screen.class));
                }
                else if (user == null) {
                    mProgressBar.setVisibility(View.GONE);
                    Toast.makeText(this, "Please try again!", Toast.LENGTH_SHORT).show();
                }

            } else if(!isConnected(this)) {
                Toast.makeText(this, "Failed to login, Please check your internet connection!", Toast.LENGTH_SHORT).show();
            }
            else {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(this, "Login failed! Please check email and password.", Toast.LENGTH_SHORT).show();
            }
        });
    }
}