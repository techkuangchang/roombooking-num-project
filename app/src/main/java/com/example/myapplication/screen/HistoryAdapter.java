package com.example.myapplication.screen;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.daimajia.swipe.SwipeLayout;
import com.example.myapplication.R;
import com.example.myapplication.model.BookingRequest;
import com.example.myapplication.model.HallRoom;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    static final String TAG = "HistoryAdapter";

    FirebaseUser currentUser;
    FirebaseDatabase mDatabase;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference mReference;

    String uuid;

    private List<BookingRequest> bookingRequestList = new ArrayList<>();
    private Context context;

    public HistoryAdapter(List<BookingRequest> bookingRequestList, Context context) {
        this.bookingRequestList = bookingRequestList;
        this.context = context;
    }

    @NonNull
    @Override
    public HistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_custom_history, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryAdapter.MyViewHolder holder, int position) {
        BookingRequest bookingRequest = bookingRequestList.get(position);
        holder.mTxtRoomName.setText(bookingRequest.getRoomNameReq());
        holder.mTxtStartTime.setText(bookingRequest.getStartTimeReq());
        holder.mTxtEndTime.setText(bookingRequest.getEndTimeReq());
        holder.mTxtDateBooking.setText(bookingRequest.getDateReq());

        /*
        TODO: Swipe Right to Delete
         */
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper));
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        holder.mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFirebaseAuth = FirebaseAuth.getInstance();
                currentUser = mFirebaseAuth.getCurrentUser();
                mFirebaseAuth.getCurrentUser();

                if (currentUser != null) {
                    uuid = currentUser.getUid();
                }

                mReference = FirebaseDatabase.getInstance().getReference("User")
                        .child(uuid).child("History");
                mReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {

                        snapshot.getRef().removeValue();

                        Toast.makeText(context, "Booking deleted.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(context, "Booking can't cancel!", Toast.LENGTH_LONG).show();
                    }
                });
                System.out.println("Delete click");
            }
        });
    }

    @Override
    public int getItemCount() {
        if(bookingRequestList == null) {
            return 0;
        }
        else {
            return bookingRequestList.size();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtRoomName, mTxtStartTime, mTxtEndTime, mTxtDateBooking;
        public SwipeLayout swipeLayout;
        public ImageButton mDelete;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mTxtRoomName = itemView.findViewById(R.id.history_txt_room);
            mTxtStartTime = itemView.findViewById(R.id.history_txt_start_time);
            mTxtEndTime = itemView.findViewById(R.id.history_txt_end_time);
            mTxtDateBooking = itemView.findViewById(R.id.history_txt_booking_date);
            swipeLayout = itemView.findViewById(R.id.swipLayout);
            mDelete = itemView.findViewById(R.id.delete_btn);
        }
    }
}
