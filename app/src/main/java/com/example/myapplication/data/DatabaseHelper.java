package com.example.myapplication.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(@Nullable Context context) {
        super(context, "Signup.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    db.execSQL("CREATE TABLE User (FullName TEXT PRIMARY KEY,Email TEXT,Password TEXT,Phone Text )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS User");
    }
    public Boolean insertDB(String fullName,String email,String password,String phone){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FullName",fullName);
        contentValues.put("Email",email);
        contentValues.put("Password",password);
        contentValues.put("Phone",phone);
        long ins = sqLiteDatabase.insert("User",null,contentValues);
        if(ins==-1){
            return false;
        }else {
            return true;
        }
    }
    public Boolean checkMail(String email){
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM User WHERE Email=?",new String[]{email});
        if (cursor.getCount()>0){
            return false;
        }else
            return true;
    }
    public Boolean Login(String email,String password){
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT * FROM User WHERE Email=? AND Password=?",new String[]{email,password});
        if(cursor.getCount()>0){
            return true;
        }else {
            return false;
        }
    }
}
