package com.example.myapplication.model;

public class BookingRequest {

    private String roomNameReq;
    private String startTimeReq;
    private String endTimeReq;
    private String DateReq;

    public BookingRequest() {
    }

    public BookingRequest(String roomNameReq, String startTimeReq, String endTimeReq, String dateReq) {
        this.roomNameReq = roomNameReq;
        this.startTimeReq = startTimeReq;
        this.endTimeReq = endTimeReq;
        DateReq = dateReq;
    }

    public String getRoomNameReq() {
        return roomNameReq;
    }

    public void setRoomNameReq(String roomNameReq) {
        this.roomNameReq = roomNameReq;
    }

    public String getStartTimeReq() {
        return startTimeReq;
    }

    public void setStartTimeReq(String startTimeReq) {
        this.startTimeReq = startTimeReq;
    }

    public String getEndTimeReq() {
        return endTimeReq;
    }

    public void setEndTimeReq(String endTimeReq) {
        this.endTimeReq = endTimeReq;
    }

    public String getDateReq() {
        return DateReq;
    }

    public void setDateReq(String dateReq) {
        DateReq = dateReq;
    }

    @Override
    public String toString() {
        return "BookingRequest{" +
                ", roomNameReq='" + roomNameReq + '\'' +
                ", startTimeReq='" + startTimeReq + '\'' +
                ", endTimeReq='" + endTimeReq + '\'' +
                ", DateReq='" + DateReq + '\'' +
                '}';
    }
}
