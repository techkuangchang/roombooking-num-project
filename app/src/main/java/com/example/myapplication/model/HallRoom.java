package com.example.myapplication.model;

import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class HallRoom {

    private String roomType;
    private String roomName;
    private String hallName;
    private String capacity;
    private int image;

    public HallRoom() {
    }

    public HallRoom(String roomType, String roomName, String hallName, String capacity, int image) {
        this.roomType = roomType;
        this.roomName = roomName;
        this.hallName = hallName;
        this.capacity = capacity;
        this.image = image;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "HallRoom{" +
                ", roomType='" + roomType + '\'' +
                ", roomName='" + roomName + '\'' +
                ", hallName='" + hallName + '\'' +
                ", capacity='" + capacity + '\'' +
                ", image=" + image +
                '}';
    }

}
